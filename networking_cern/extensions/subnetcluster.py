#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#
# pylint: disable=no-self-use,arguments-differ

"""
This module holds the definition of the CERN subnet cluster extension.
"""

import abc

from neutron.api import extensions as neutron_extensions
from neutron.api.v2 import resource_helper
from neutron_lib.api import converters
from neutron_lib.api import extensions
from neutron_lib.services import base as service_base
from networking_cern.common import constants
from networking_cern import extensions as cern_extensions

# Ensure the extension is loaded at startup
neutron_extensions.append_api_extensions_path(cern_extensions.__path__)

RESOURCE_NAME = 'cluster'

RESOURCE_ATTRIBUTE_MAP = {
    RESOURCE_NAME + 's': {
        'id': {'allow_post': False, 'allow_put': False,
               'validate': {'type:uuid': None},
               'is_visible': True},
        'name': {'allow_post': True, 'allow_put': True,
                 'validate': {'type:string': None},
                 'is_visible': True},
        'tenant_id': {'allow_post': True, 'allow_put': False,
                      'required_by_policy': True,
                      'validate': {'type:string': None},
                      'is_visible': True},
        'subnets': {'allow_post': True, 'allow_put': True,
                    'validate': {'type:uuid_list': None},
                    'convert_to': converters.convert_none_to_empty_list,
                    'default': None, 'is_visible': True},
    },
}


class Subnetcluster(extensions.ExtensionDescriptor):
    """Definition of the subnet cluster extension."""

    @classmethod
    def get_name(cls):
        """Returns the extension's name."""
        return "Subnet Cluster"

    @classmethod
    def get_alias(cls):
        """Returns the extension's alias."""
        return "subnetcluster"

    @classmethod
    def get_description(cls):
        """Returns the extension's description."""
        return "Allows grouping of subnets into clusters"

    @classmethod
    def get_namespace(cls):
        """Returns the extension's namespace."""
        return "http://github.com/cernops/neutron"

    @classmethod
    def get_updated(cls):
        """Returns the time of the extension's last update."""
        return "2015-01-15T10:00:00-00:00"

    @classmethod
    def get_resources(cls):
        """Returns Ext Resources."""

        special_mappings = {'clusters': 'cluster'}
        plural_mappings = resource_helper.build_plural_mappings(
            special_mappings, RESOURCE_ATTRIBUTE_MAP)
        action_map = {'cluster': {'insert_subnet': 'PUT',
                                  'remove_subnet': 'PUT'}}
        return resource_helper.build_resource_info(plural_mappings,
                                                   RESOURCE_ATTRIBUTE_MAP,
                                                   constants.CERN,
                                                   action_map=action_map)

    @classmethod
    def get_plugin_interface(cls):
        """Returns the base plugin class."""
        return SubnetClusterPluginBase

    def update_attributes_map(self, attributes):
        """Updates the extension's attr map."""
        super(Subnetcluster, self).update_attributes_map(
            attributes, extension_attrs_map=RESOURCE_ATTRIBUTE_MAP)

    def get_extended_resources(self, version):
        """Returns the extended resources for the given version."""
        if version == "2.0":
            return RESOURCE_ATTRIBUTE_MAP
        return {}


class SubnetClusterPluginBase(service_base.ServicePluginBase):

    """REST API to operate the Subnet Clusters.

    All of method must be in an admin context.
    """

    @abc.abstractmethod
    def create_cluster(self, context, cluster):
        """Create a new cluster."""
        pass

    @abc.abstractmethod
    def delete_cluster(self, context, cluster_id):
        """Delete the cluster with the given id."""
        pass

    @abc.abstractmethod
    def update_cluster(self, context, cluster_id, cluster):
        """Update the cluster with the given id."""
        pass

    @abc.abstractmethod
    def get_clusters(self, context, filters=None, fields=None):
        """Return clusters corresponding to the given filters or fields."""
        pass

    @abc.abstractmethod
    def get_cluster(self, context, cluster_id, fields=None):
        """Return the cluster with the given id."""
        pass

    @abc.abstractmethod
    def insert_subnet(self, context, cluster_id, body):
        """Insert the given subnet in the given cluster id."""
        pass

    @abc.abstractmethod
    def remove_subnet(self, context, cluster_id, body):
        """Remove the given subnet from the given cluster_id."""
        pass
