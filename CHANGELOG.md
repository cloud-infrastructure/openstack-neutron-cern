# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Types of changes are: `added`, `changed`, `deprecated`, `removed`, `fixed` or `security`.

## [Unreleased]

## [0.16.1] - 2024-10-28

### Changed

-  Ensure there is only one call to nova while adding port to device  (!71)

## [0.16.0] - 2024-10-01

### Changed

- Refactored helper methods  (!69)
- Use cached landb_client for reducing auth calls to LanDB  (!69)

### Added

- Dry run configuration setting to not trigger real calls to LanDB (!69)
- Optional operation mode called "landb_refactor" that handles exclusively 
  port related interactions with LanDB (!69)

## [0.15.4] - 2024-08-15

### Changed

- Build using new CI
- Fix network cluster subnet removal and deletion

## [0.15.3] - 2023-12-14

### Changed

- Add support for querying segments API to get LanDB cluster information (!64)

## [0.15.2] - 2023-02-17

### Changed

- Rename tenant_id column in cern_clusters to project_id

### Added

- Configuration parameter `xldap_url` to customise the LDAP endpoint to use (OS-16344)

## [0.15.1] - 2022-10-07

### Changed

- Replace deprecated "auth-uri" by "www_authenticate_uri" (!60)

## [0.15.0] - 2022-07-13

### Changed

- Adapted to use neutron_lib
- Bumped Neutron dependency to Train

## [0.14.0] - 2022-06-23

### Added

- Added `most_available_subnet_v{4,6}` to host restrictions API (!54)

## [0.13.1] - 2022-05-19

### Changed

- Changed type of option for verify (!51)

### Fixed

- Fixed UnboundLocalError: local variable instance referenced before assignment (!51)
