from keystoneauth1 import loading
from keystoneauth1 import session
from keystoneclient.v3 import client as keystone_client

class Keystone(object):
    """Defines all required Keystone interaction."""

    def __init__(self, sess):
        self.client = keystone_client.Client(session=sess)

    def _get_project(self, project_id):
        try:
            project = self.client.projects.get(project_id)
        except Exception as exc:
            LOG.error("cern-landb: failed to get project %s :: %s"
                      % (project_id, exc))
            raise ml2_exc.MechanismDriverError(method='keystone')
        return project

    def get_project_mainuser(self, project_id):
        """Returns the main user of the given project."""
        project = self._get_project(project_id)
        mainuser = None
        if project and 'landb-mainuser' in project.__dict__:
            mainuser = project.__dict__['landb-mainuser']
        return mainuser

    def get_project_responsible(self, project_id):
        """Returns the responsible for the given project."""
        project = self._get_project(project_id)
        responsible = None
        if project and 'landb-responsible' in project.__dict__:
            responsible = project.__dict__['landb-responsible']
        return responsible

