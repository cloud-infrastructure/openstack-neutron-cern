# Copyright (c) 2014 CERN
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

"""
Implements interactions with the CERN ldap server.
"""

import ldap

from neutron._i18n import _
from oslo_log import log as logging

from networking_cern.common import cern_exceptions as exception


LOG = logging.getLogger(__name__)


class Xldap(object):
    """Holds all interactions with the CERN LDAP backend."""
    def __init__(self, url='ldap://xldap.cern.ch', protocol_version=ldap.VERSION3,
                 searchScope=ldap.SCOPE_SUBTREE, retrieveAttributes=None):
        self.client = ldap.initialize(url)
        self.client.protocol_version = protocol_version
        self.search_scope = searchScope
        self.retrieve_attributes = retrieveAttributes

    def user_exists(self, user,
                    base_dn='OU=Users,OU=Organic Units,DC=cern,DC=ch'):
        """Check if an user exists at CERN"""
        try:
            search_filter = "cn=" + user
            ldap_result_id = self.client.search(
                base_dn, self.search_scope, search_filter,
                self.retrieve_attributes)
            result_type, result_data = self.client.result(ldap_result_id, 0)
            if result_data == []:
                return False
            if result_type == ldap.RES_SEARCH_ENTRY:
                return int(result_data[0][1]['employeeID'][0])
        except Exception as exc:
            LOG.error(_("Cannot verify if USER exists. %s" % str(exc)))
            raise exception.CernInvalidUser()
        return False

    def egroup_exists(self, egroup, base_dn='OU=Workgroups,DC=cern,DC=ch'):
        """Check if an egroup exists at CERN"""
        try:
            search_filter = "cn=" + egroup
            ldap_result_id = self.client.search(
                base_dn, self.search_scope, search_filter,
                self.retrieve_attributes)
            result_type, result_data = self.client.result(ldap_result_id, 0)
            if result_data == []:
                return False
            if result_type == ldap.RES_SEARCH_ENTRY:
                return str(egroup)
        except Exception as exc:
            LOG.error(_("Cannot verify if EGROUP exists. %s" % str(exc)))
            raise exception.CernInvalidEgroup()
        return False

    def device_exists(self, device, base_dn='OU=Workgroups,DC=cern,DC=ch'):
        """Check if device exists in Xldap"""
        try:
            search_filter = "(&(name=" + device + "))"
            ldap_result_id = self.client.search(
                base_dn, self.search_scope, search_filter,
                self.retrieve_attributes)
            result_type, result_data = self.client.result(ldap_result_id, 0)
            if result_data == []:
                return False
            if result_type == ldap.RES_SEARCH_ENTRY:
                return device
        except Exception as exc:
            LOG.error(_("Cannot verify if device exists. %s" % str(exc)))
            raise exception.CernInvalidDevice()
        return False
