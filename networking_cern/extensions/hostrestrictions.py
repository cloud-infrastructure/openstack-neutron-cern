#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#
# pylint: disable=no-self-use

"""
This module defines the Host Restrictions CERN extension.
"""

import abc

from neutron.api import extensions as neutron_extensions
from neutron.api.v2 import resource_helper
from neutron_lib.api import extensions
from neutron_lib.api import converters
from neutron_lib.services import base as service_base
from networking_cern.common import constants
from networking_cern import extensions as cern_extensions

# Ensure the extension is loaded at startup
neutron_extensions.append_api_extensions_path(cern_extensions.__path__)

RESOURCE_NAME = 'host'

RESOURCE_ATTRIBUTE_MAP = {
    RESOURCE_NAME + 's': {
        'hostname': {'allow_post': False, 'allow_put': False,
                     'validate': {'type:string': None},
                     'is_visible': True},
        'all_subnets': {'allow_post': False, 'allow_put': False,
                        'validate': {'type:uuid_list': None},
                        'convert_to': converters.convert_none_to_empty_list,
                        'default': None, 'is_visible': True},
        'available_subnets': {'allow_post': False, 'allow_put': False,
                              'validate': {'type:uuid_list': None},
                              'convert_to': converters.convert_none_to_empty_list,
                              'default': None, 'is_visible': True},
        'available_subnets_v6': {'allow_post': False, 'allow_put': False,
                                 'validate': {'type:uuid_list': None},
                                 'convert_to': converters.convert_none_to_empty_list,
                                 'default': None, 'is_visible': True},
        'available_random_subnet': {'allow_post': False, 'allow_put': False,
                                    'validate': {'type:string': None},
                                    'is_visible': True},
        'available_random_subnet_v6': {'allow_post': False, 'allow_put': False,
                                       'validate': {'type:string': None},
                                       'is_visible': True},
        'most_available_subnet': {'allow_post': False, 'allow_put': False,
                                  'validate': {'type:string': None},
                                  'is_visible': True},
        'most_available_subnet_v4': {'allow_post': False, 'allow_put': False,
                                  'validate': {'type:string': None},
                                  'is_visible': True},
        'most_available_subnet_v6': {'allow_post': False, 'allow_put': False,
                                  'validate': {'type:string': None},
                                  'is_visible': True},
        'least_available_subnet': {'allow_post': False, 'allow_put': False,
                                   'validate': {'type:string': None},
                                   'is_visible': True},
    },
}


class Hostrestrictions(extensions.ExtensionDescriptor):
    """
    Defines the Host Restrictions extension.
    """

    @classmethod
    def get_name(cls):
        """Returns the extension's name."""
        return "CERN Host Restrictions"

    @classmethod
    def get_alias(cls):
        """Returns the extension's alias."""
        return "hostrestrictions"

    @classmethod
    def get_description(cls):
        """Returns the extension's description."""
        return "Returns information about CERN network host restrictions"

    @classmethod
    def get_namespace(cls):
        """Returns the extension's namespace."""
        return "http://github.com/cernops/neutron"

    @classmethod
    def get_updated(cls):
        """Returns the last update of this extension."""
        return "2015-03-09T10:00:00-00:00"

    @classmethod
    def get_resources(cls):
        """Returns Ext Resources."""

        special_mappings = {'hosts': 'host'}
        plural_mappings = resource_helper.build_plural_mappings(
            special_mappings, RESOURCE_ATTRIBUTE_MAP)
        action_map = {}
        return resource_helper.build_resource_info(plural_mappings,
                                                   RESOURCE_ATTRIBUTE_MAP,
                                                   constants.CERN,
                                                   action_map=action_map)

    @classmethod
    def get_plugin_interface(cls):
        """Returns the base class implementation for the extension."""
        return HostRestrictionsPluginBase

    def get_extended_resources(self, version):
        """Returns the extension according to the version."""
        if version == "2.0":
            return RESOURCE_ATTRIBUTE_MAP
        return {}


class HostRestrictionsPluginBase(service_base.ServicePluginBase):
    """REST API to operate the host restrictions.

    All of method must be in an admin context.
    """

    @abc.abstractmethod
    def get_host(self, context, host_id, fields=None):
        """Returns the host for the given id."""
        pass
