# Copyright (c) 2015-2017 CERN
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#
# pylint: disable=no-self-use,protected-access,no-value-for-parameter

"""Implementation of the CERN LanDB ml2 driver."""

from neutron_lib.plugins.ml2 import api
from neutron.plugins.ml2.common import exceptions as ml2_exc
from glanceclient import Client as glance_client
from keystoneauth1 import loading
from keystoneauth1 import session
from oslo_config import cfg
from oslo_log import log
from neutronclient.v2_0 import client as neutron_client
from novaclient import client as nova_client
from landbclient import client as networkdb_client
from webob import exc as web_exc

from networking_cern.common import cernlib as cern
from networking_cern.common import cern_exceptions as cern_exc
from networking_cern.plugins.cern import plugin as cernplugin
import networking_cern.plugins.ml2.drivers.cern.utils as cern_utils

import ipaddress
import time

LOG = log.getLogger(__name__)
CONF = cfg.CONF

VM_LOCATION = {'Floor': '0', 'Room': '0', 'Building': '0'}
VM_MANUFACTURER = 'KVM'
VM_MODEL = 'VIRTUAL MACHINE'
VM_TAG = 'OPENSTACK VM'


class CERNLanDBMechanismDriver(api.MechanismDriver):
    """Holds all required interaction with LanDB."""

    LANDB_CLIENT_MAX_AGE = 3600  # number of seconds after client should refresh

    _landb_client = None
    _landb_created = 0

    def initialize(self):
        """Initializes the plugin."""
        pass

    def _get_session(self):
        token = CONF.keystone_authtoken
        loader = loading.get_plugin_loader('password')
        auth = loader.load_from_options(
            auth_url=token.www_authenticate_uri,
            username=token.username,
            password=token.password,
            user_domain_name=token.user_domain_name,
            project_name=token.project_name,
            project_domain_name=token.project_domain_name)
        return session.Session(auth=auth, verify=CONF.CERN.verify)

    def _get_landbclient(self):
        if (CERNLanDBMechanismDriver._landb_client and
                ((time.time() - CERNLanDBMechanismDriver._landb_created) <
                CERNLanDBMechanismDriver.LANDB_CLIENT_MAX_AGE)):
            return CERNLanDBMechanismDriver._landb_client

        landb_client = networkdb_client.LanDB(
            username=CONF.CERN.landb_username,
            password=CONF.CERN.landb_password,
            host=CONF.CERN.landb_hostname,
            port=CONF.CERN.landb_port,
            protocol=CONF.CERN.landb_protocol,
            version=CONF.CERN.landb_version)
        CERNLanDBMechanismDriver._landb_client = landb_client
        CERNLanDBMechanismDriver._landb_created = time.time()
        return landb_client

    def _parse_dhcp_opts(self, dhcp_opts):
        result = {}
        for option in dhcp_opts:
            result[option['opt_name']] = option['opt_value']
        return result

    def _get_region(self):
        region = CONF.keystone_authtoken.region_name
        return region if region else 'cern'

    def _get_cluster(self, landb_client, context, subnet_id, subnet_name, host, neutron_client):
        if CONF.CERN.use_segments:
            service_name = subnet_name.replace("-V6", "")
            cluster_name = landb_client.cluster_by_service_host(host, service_name)
            LOG.debug(
                f"Fetched CERN network cluster through LanDB API for {subnet_name}@{host} ({cluster_name})")
            return cluster_name

        cern_plugin = cernplugin.CERNServicePlugin()
        cluster_name = cern_plugin.get_cluster_by_subnet(
            context._plugin_context, subnet_id)['name']
        LOG.debug("Fetched CERN network cluster through custom Cluster API for %s (%s)",
                  subnet_id, cluster_name)
        return cluster_name

    def _is_cern_network(self, network):
        """Check whether this is a network to consider for LanDB."""
        #is_external_network = network['router:external']
        #LOG.debug(f"sync_landb_port CERN_NETWORK? {is_cern_network}, "
        #            f"external network? {is_external_network}")

        return network['name'] == 'CERN_NETWORK'

    def sync_landb_port(self, context, operation):
        current = context.current
        original = context.original
        network = context.network.current

        is_cern_network = self._is_cern_network(network)
        device_owner = current['device_owner'] if current else None
        if not device_owner or not ':' in device_owner:
            device_owner = original['device_owner'] if original else ''

        #current_dns_name = ''
        #if current and 'dns_name' in current:
        #    current_dns_name = current['dns_name']
        #original_dns_name = ''
        #if original and 'dns_name' in original:
        #    original_dns_name = original['dns_name']
        current_device_id = ''
        if current and 'device_id' in current:
            current_device_id = current['device_id']
        original_device_id = ''
        if original and 'device_id' in original:
            original_device_id = original['device_id']

        if current_device_id != original_device_id:
            # Device changed
            LOG.info("different device_id")
            if (device_owner.split(':')[0] == 'compute'):
                # This is a nova device, so VM in our case
                if operation == "delete":
                    # Port is being detached
                    self._remove_port_from_device(
                        current, context, remove_interface=is_cern_network)

                elif len(current_device_id) == 0 and len(original_device_id) > 0:
                    # Port is being detached
                    self._remove_port_from_device(
                        original, context, remove_interface=is_cern_network)

                elif len(current_device_id) > 0 and len(original_device_id) == 0:
                    # Port is being attached and all information should be there
                    self._add_port_to_device(
                        current, context, register_interface=is_cern_network)

            elif (device_owner.split(':')[0] == 'network'):
                # This is a neutron device.
                # We have to create the device on our own if necessary
                LOG.warn("CERN LanDB: DeviceOwner network not implemented")

            else:
                # We have to create the device on our own if necessary
                LOG.warn(f"CERN LanDB: DeviceOwner unknown: {device_owner}")

    def _add_port_to_device(self, port, context, register_interface=True):
        ipv4s = []
        ipv6s = []
        subnet_names = []
        #segment_id = None
        mac_address = port['mac_address']
        device_name = ''
        device = None
        if 'dns_name' in port:
            device_name = port['dns_name']
        if len(device_name) < 1:
            device = cern_utils._get_instance(self._get_session(),
                port['device_id'], self._get_region())
            device_name = device.name

        interface_name = port['name']
        if len(interface_name) < 1:
            interface_name = device_name

        if register_interface:
            # Do validation before everything else
            for fixed_ip in port["fixed_ips"]:
                subnet_id = fixed_ip["subnet_id"]
                subnet_info = context._plugin.get_subnet(
                    context._plugin_context,
                    subnet_id)
                s_name = subnet_info["name"]
                subnet_names.append(
                    s_name[:-3] if s_name.endswith('-V6') else s_name)
                #segment_id = subnet_info["subnet"]["segment_id"]

                address = fixed_ip["ip_address"]
                if ipaddress.ip_address(address).version == 4:
                    ipv4s.append(address)
                else:
                    ipv6s.append(address)

            # Check that we have a single subnet
            if len(set(subnet_names)) > 1:
                raise Exception(
                    "Not allowed to have multiple subnet names on "
                    "single interface")

            # Check that we have a single ipv4
            if len(ipv4s) != 1:
                raise Exception(
                    "There should be exactly one IPv4 configured.")

            # Check that we have a single ipv6
            if len(ipv6s) != 1:
                raise Exception(
                    "There should be exactly one IPv6 configured.")

        try:
            client_landb = self._get_landbclient()
            LOG.debug(f"client_landb.device_addcard({device_name}, {mac_address})")
            if not CONF.CERN.dry_run:
                client_landb.device_addcard(device_name, mac_address)
            if not register_interface:
                # If it is a private network that we don't register in LanDB,
                # only register the MAC address but ignore the IPs
                return

            subnet_name = subnet_names[0]
            ipv4 = ipv4s[0] if len(ipv4s) > 0 else None
            ipv6 = ipv6s[0] if len(ipv6s) > 0 else None

            move_interface = "landb-move-interface" in port["tags"]

            if move_interface:
                LOG.debug(f"landb.device_moveinterface({device_name}, "
                          f"{interface_name})")
                if not CONF.CERN.dry_run:
                    client_landb.device_moveinterface(device_name,
                                                      interface_name)
            else:
                if not device:
                    # Only ask for the device again, if it wasn't asked for before
                    device = cern_utils._get_instance(self._get_session(),
                        port['device_id'], self._get_region())
                interface_fqdn = interface_name.upper() + ".CERN.CH"
                internet = cern_utils._parse_landb_alias_and_set_string(
                    device.metadata.get('landb-internet-connectivity', ''),
                    device_name)
                internet_set = internet.get(interface_fqdn, set())
                internet = (len(internet_set) == 0 or
                            'TRUE' in internet_set)

                LOG.debug("client_landb.device_addinterface(%r", [
                        device_name, interface_name, ipv4, ipv6, subnet_name,
                        internet, context.host])
                if not CONF.CERN.dry_run:
                    client_landb.device_addinterface(
                        device_name, interface_name, ipv4, ipv6, subnet_name,
                        internet, parent=context.host)

                # Add sets/alias for the interface
                if register_interface and not move_interface:
                    sets = cern_utils._parse_landb_alias_and_set_string(
                        device.metadata.get('landb-set',''),
                        device_name)
                    alias = cern_utils._parse_landb_alias_and_set_string(
                        device.metadata.get('landb-alias',''),
                        device_name)
                    # Add sets and alias
                    client_landb = self._get_landbclient()
                    if interface_fqdn in sets:
                        for set_name in sets[interface_fqdn]:
                            LOG.debug('client_landb.update_address_set(%r' % [
                                set_name, device.tenant_id, interface_fqdn])
                            if not CONF.CERN.dry_run:
                                client_landb.update_address_set(set_name,
                                    device.tenant_id, interface_fqdn)
                    if (interface_fqdn in alias and
                        len(alias[interface_fqdn]) > 0):
                        LOG.debug("client_landb.alias_update(%r" % [
                            interface_fqdn, alias[interface_fqdn]])
                        if not CONF.CERN.dry_run:
                            client_landb.alias_update(
                                interface_fqdn, alias[interface_fqdn])

            LOG.debug(f"client_landb.bind_unbind_interface({interface_name}, "
                      f"{mac_address})")
            if not CONF.CERN.dry_run:
                try:
                    client_landb.bind_unbind_interface(interface_name,
                        mac_address)
                except:
                    # If bind_unbind fails, it is not good, but should also
                    # not prohibit the further execution
                    pass
        except Exception as e:
            LOG.error("Error during attaching of interface/card at device: "
                      "%s, mac: %s",  device_name, mac_address)
            LOG.error("Cannot add card/interface to LanDB for device %s: %s",
                      device_name, e)
            raise e

    def _remove_port_from_device(self, port, context, remove_interface):
        device_name = ''
        if 'dns_name' in port:
            device_name = port['dns_name']
        if len(device_name) < 1:
            device = cern_utils._get_instance(self._get_session(),
                port['device_id'], self._get_region())
            device_name = device.name
        mac_address = port['mac_address']
        ip_address = port["fixed_ips"][0]["ip_address"]
        interface_name = port['name']
        if len(interface_name) < 1:
            interface_name = device_name

        keep_interface = "landb-move-interface" in port["tags"]

        try:
            client_landb = self._get_landbclient()
            if not keep_interface and remove_interface:
                LOG.debug(f"client_landb.device_removeinterface("
                          f"{device_name}, {interface_name})")
                if not CONF.CERN.dry_run:
                    client_landb.device_removeinterface(device_name,
                                                        interface_name,
                                                        ip_address)

            LOG.debug(f"client_landb.device_removecard({device_name}, "
                      f"{mac_address})")
            if not CONF.CERN.dry_run:
                client_landb.device_removecard(device_name, mac_address)
        except Exception as e:
            LOG.error("Error during removal of interface/card at device: %s, "
                      "IP: %s, mac: %s",  device_name, ip_address, mac_address)
            LOG.error("Cannot remove card/interface from LanDB for device "
                      "%s: %s", device_name, e)
            raise e

    def create_port_precommit(self, context):
        """
        Triggered before the successful creation of a Neutron port.
        """
        # Validate the port name
        if self._is_cern_network(context.network.current):
            port_name = context.current["name"]
            self.validate_hostname(port_name)

    def validate_hostname(self, name):
        if not name or len(name) == 0:
            return

        name = name.lower()
        if len(name) > 64:
            msg = "Port name too long"
            raise web_exc.HTTPBadRequest(explanation=msg)

        # TODO(dfailing) maybe just replace with regex
        # possibility: [a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9]
        valid_character = "abcdefghijklmnopqrstuvwxyz0123456789-"
        # alternative: nova.utils.sanitize_hostname(name) != name
        if len(set(name) - set(valid_character)) > 0:
            # Name has to be alphanumerical or has a -
            msg = "Port name is not a valid hostname"
            raise web_exc.HTTPBadRequest(explanation=msg)

        if not name[0].isalpha():
            # Name has to begin with character
            msg = "Port name is not a valid hostname"
            raise web_exc.HTTPBadRequest(explanation=msg)

        if name[-1] == '-':
            # Name is not allowed to end with -
            msg = "Port name is not a valid hostname"
            raise web_exc.HTTPBadRequest(explanation=msg)

        client_landb = self._get_landbclient()
        if client_landb.device_exists(name):
            msg = "Hostname already in use"
            raise web_exc.HTTPBadRequest(explanation=msg)

    def create_port_postcommit(self, context):
        """
        Triggered after the successful creation of a Neutron port.

        Checks whether changes to LanDB have to be performed.
        """
        super(CERNLanDBMechanismDriver, self).create_port_postcommit(context)
        # Raising MechanismDriverError here will delete the port from neutron
        try:
            if CONF.CERN.landb_refactor:
                self.sync_landb_port(context, "create")
            else:
                self.pre_refactor_create_port_postcommit(context)
        except Exception as e:
            raise ml2_exc.MechanismDriverError(method='landb-create')

    def update_port_precommit(self, context):
        """
        Triggered during an port update, like a port binding/attachment/detachment.

        Checks whether changes to LanDB have to be performed.
        """
        super(CERNLanDBMechanismDriver, self).update_port_precommit(context)
        # Raising an exception here will prevent
        # the port from being changed

        # First check that the user is not trying to change the port name
        orig_name = None
        if context.original and 'name' in context.original:
            orig_name = context.original['name']

        cur_name = None
        if context.current and 'name' in context.current:
            cur_name = context.current['name']

        if cur_name is not None and orig_name is not None:
            if cur_name != orig_name:
                msg = "Not allowed to change port name"
                raise web_exc.HTTPBadRequest(explanation=msg)

        try:
            if CONF.CERN.landb_refactor:
                self.sync_landb_port(context, "update")
        except Exception as e:
            raise ml2_exc.MechanismDriverError(method='landb-update')

    def delete_port_postcommit(self, context):
        """
        Triggered after the successful deletion of a Neutron port.

        Checks whether changes to LanDB have to be performed.
        """
        super(CERNLanDBMechanismDriver, self).delete_port_postcommit(context)
        # Raising an exception here will NOT prevent
        # the port from being deleted
        try:
            if CONF.CERN.landb_refactor:
                self.sync_landb_port(context, "delete")
            else:
                self.pre_refactor_delete_port_postcommit(context)
        except Exception as e:
            raise ml2_exc.MechanismDriverError(method='landb-delete')

    ### Old integration
    def pre_refactor_create_port_postcommit(self,context):
        if context.current['device_owner'].split(':')[0] == 'compute':
            sess = self._get_session()

            landb_client = self._get_landbclient()
            instance = None
            ip_addr = None
            mac_addr = None
            parent = None
            ip_service = None

            instance = cern_utils._get_instance(
                sess, context.current['device_id'], self._get_region())

            dhcp_opts = {}
            neutron = neutron_client.Client(
                    session=sess,
                    region_name=self._get_region())

            if not instance.image:
                port = neutron.show_port(context.current['id'])
                if port:
                    dhcp_opts = self._parse_dhcp_opts(
                        port['port']['extra_dhcp_opts'])

            metadata = cern_utils._get_instance_metadata(
                sess, instance, dhcp_opts, self._get_region())
            ip_addr = context.current['fixed_ips'][0]['ip_address']
            ip6_addr = None
            if len(context.current['fixed_ips']) > 1:
                ip6_addr = context.current['fixed_ips'][1]['ip_address']
            mac_addr = context.current['mac_address']
            parent = context.current['binding:host_id'].split('.')[0]
            subnet_id = context.current['fixed_ips'][0]['subnet_id']
            ip_service = context._plugin.get_subnet(
                context._plugin_context,
                subnet_id)['name']
            vm_landb_manager = {'FirstName': 'E-GROUP',
                                'Name': CONF.CERN.landb_manager}
            try:
                landb_client.vm_create(
                    vm_name=instance.name,
                    vm_location=VM_LOCATION,
                    vm_manufacturer=VM_MANUFACTURER,
                    vm_model=VM_MODEL,
                    vm_description=metadata['description'],
                    vm_os=metadata['os'],
                    vm_responsible=metadata['responsible'],
                    vm_landb_manager=vm_landb_manager,
                    vm_mac=mac_addr,
                    vm_cluster=self._get_cluster(landb_client, context, subnet_id, ip_service, parent, neutron),
                    vm_parent=parent,
                    vm_tag=VM_TAG,
                    vm_user=metadata['mainuser'],
                    vm_ip=ip_addr,
                    vm_ipv6=ip6_addr,
                    vm_ipservice=ip_service,
                    vm_internet=metadata['connectivity'],
                    vm_alias=metadata['alias'],
                    ipv6ready=metadata['ipv6ready'],
                    manager_locked=True
                )

                LOG.info("cern-landb: instance created %s"
                        % {'instance': instance.name})
            except Exception as exc:
                device = instance.name if hasattr(instance, 'name') else None
                LOG.error(
                    "cern-landb: failed to create entry :: %s"
                    % {'device': device, 'ip': ip_addr, 'mac': mac_addr,
                    'hv': parent, 'ips': ip_service, 'msg': exc})
                raise ml2_exc.MechanismDriverError(method='landb-create')

            set_name = cern_utils._get_set(instance)
            if set_name:
                interface_name = instance.name + ".CERN.CH"
                try:
                    landb_client.update_address_set(set_name,
                                                    instance.tenant_id,
                                                    interface_name)
                except Exception as exc:
                    LOG.error(
                        "cern-landb: failed to add interface to set :: %s"
                        % {'device': instance.name, 'ip': ip_addr, 'msg': exc})
        else:
            LOG.error(
                "cern-landb: attempted creation of port with owner not "
                "nova: %s" % {'own': context.current['device_owner']})

    def pre_refactor_delete_port_postcommit(self, context):
        """
        Triggered after the successful deletion of a Neuton port.

        Performs the required deletion on CERN LanDB.
        """
        # Raising an exception here will NOT prevent
        # the port from being deleted
        if context.current['device_owner'].split(':')[0] == 'compute':
            ip_address = context.current['fixed_ips'][0]['ip_address']

            landb_client = self._get_landbclient()
            try:
                instance = landb_client.device_search(ip_address)
                if instance and len(instance) >= 1:
                    landb_client.vm_delete(instance[0])
                    LOG.info("cern-landb: instance deleted :: %s"
                             % {'instance': instance[0]})
                else:
                    LOG.warn("cern-landb: instance is not correct :: %s"
                             % {'instance': instance})
                    raise ml2_exc.MechanismDriverError(method='landb-delete')
            except cern_exc.CernLanDBUpdate:
                LOG.error(
                    "cern-landb: failed to delete instance %s"
                    % {'instance': instance})
                raise ml2_exc.MechanismDriverError(method='landb-delete')
            except cern_exc.CernDeviceNotFound:
                LOG.error(
                    "cern-landb: instance not found %s"
                    % {'instance': instance, 'ip': ip_address})
                raise ml2_exc.MechanismDriverError(method='landb-delete')
        else:
            LOG.error(
                "cern-landb: attempted deletion of port with "
                "owner not nova: %s"
                % {'own': context.current['device_owner']})
