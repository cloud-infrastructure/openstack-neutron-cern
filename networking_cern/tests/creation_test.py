"""CERN Neutron Plugin device test suite."""
import logging
import os
import time

from unittest import TestCase

from keystoneauth1.identity import v3
from keystoneauth1 import session
from novaclient import client as nova_client
from novaclient import exceptions

from landbclient.client import LanDB

START_TIME = int(round(time.time() * 1000))

RESPUSER = {
    'Name': 'BRITO DA ROCHA',
    'FirstName': 'RICARDO MANUEL',
    'CCID': 617758
}

MAINUSER = {
    'Name': 'BRITO DA ROCHA',
    'FirstName': 'RICARDO MANUEL',
    'CCID': 617758
}


def _get_session():
    auth = v3.Password(
        auth_url=os.environ['OS_AUTH_URL'], username=os.environ['OS_USERNAME'],
        password=os.environ['OS_PASSWORD'],
        project_name=os.environ['OS_PROJECT_NAME'],
        user_domain_id=os.environ['OS_DEFAULT_DOMAIN_NAME'],
        project_domain_id=os.environ['OS_DEFAULT_DOMAIN_NAME'])
    return session.Session(auth=auth)


def _sample_device_info(
        device, description=None, connectivity=True, alias=None,
        respuser=RESPUSER, mainuser=MAINUSER, osystem='LINUX', osversion='7',
        ipv6ready=True):
    return {
        'Location': {
            'Building': '0000', 'Floor': '0', 'Room': '0000'
        },
        'Zone': '0000', 'Manufacturer': 'KVM', 'Model': 'VIRTUAL MACHINE',
        'GenericType': 'COMPUTER', 'Description': description,
        'Tag': 'OPENSTACK VM',
        'OperatingSystem': {
            'Name': osystem, 'Version': osversion,
        },
        'ResponsiblePerson': respuser,
        'UserPerson': mainuser,
        'IPv6Ready': ipv6ready,
        'NetworkInterfaceCards': [
            {
                'HardwareAddress':
                device.addresses['CERN_NETWORK'][0][
                    'OS-EXT-IPS-MAC:mac_addr'].replace(':', '-').upper()
            }
        ],
        'Interfaces': [
            {
                'IPAddress': device.addresses['CERN_NETWORK'][0]['addr'],
                'InternetConnectivity': connectivity,
                'IPAliases': alias,
            }
        ],
    }


class TestDevice(TestCase):  # pylint: disable=too-many-instance-attributes
    """Device management tests."""

    def setUp(self):
        logging.getLogger('suds').setLevel(logging.INFO)
        logging.captureWarnings(True)
        self.image = os.environ['IMG'] if 'IMG' in os.environ else 'cc7'
        self.flavor = os.environ['FLAVOR'] if 'FLAVOR' in os.environ\
            else 'm2.small'
        self.avz = os.environ['AZ'] if 'AZ' in os.environ else None
        self.key = os.environ['KEY'] if 'KEY' in os.environ else None
        self.meta = {'cern-services': 'false'}

        sess = _get_session()
        self.nova = nova_client.Client('2', session=sess)
        self.landb = LanDB(
            'vmmaster', os.environ['LANDB_PASSWORD'],
            'network.cern.ch', '443', None, None, None, 'https')

    def _create_vm(self, device):
        image = self.nova.images.find(name=device['image'])
        flavor = self.nova.flavors.find(name=device['flavor'])
        self.nova.servers.create(
            device['name'], image=image.id, flavor=flavor.id,
            meta=device['meta'], key_name=device['key'],
            availability_zone=device['avz'],
            block_device_mapping_v2=device['blk_mapping'])

        result = self.nova.servers.list(
            search_opts={'name': device['name']})
        while not result or result[0].status == 'BUILD':
            time.sleep(2)
            result = self.nova.servers.list(
                search_opts={'name': device['name']})

    def _get_vm(
            self, name, image=None, flavor=None, meta=None, key=None,
            avz=None, blk_mapping=None):
        image = image if image else self.image
        flavor = flavor if flavor else self.flavor
        meta = meta if meta else self.meta
        key = key if key else self.key
        avz = avz if avz else self.avz

        return {'name': name, 'image': image, 'flavor': flavor, 'meta': meta,
                'key': key, 'avz': avz, 'blk_mapping': blk_mapping}

    def _delete_vm(self, name):
        try:
            result = self.nova.servers.list(
                search_opts={'name': name})
            if result:
                for device in result:
                    self.nova.servers.delete(device.id)
                result = self.nova.servers.list(
                    search_opts={'name': name})
                while result:
                    time.sleep(2)
                    result = self.nova.servers.list(
                        search_opts={'name': name})
        except exceptions.NotFound:
            pass

    def _assert_vm(self, result, expected):
        # image = self.nova.images.find_image(result.image)
        # flavor = self.nova.flavors.find(id=result.flavor)
        # self.assertEqual(image.name, expected['image'])
        # self.assertEqual(flavor.name, expected['flavor'])
        # self.assertEqual(result.availability_zone, expected['avz'])
        self.assertEqual(result.name, expected['name'])
        self.assertEqual(result.key_name, expected['key'])
        self.assertEqual(len(result.metadata), len(expected['meta']))
        for key in result.metadata.keys():
            self.assertEqual(result.metadata[key], expected['meta'][key])

    def _assert_landb(self, result, expected):
        device_info = self.landb.device_info(result.name)
        self.assertEquals(device_info['DeviceName'].lower(), result.name)
        self.assertEquals(
            device_info['Location']['Building'],
            expected['Location']['Building'])
        self.assertEquals(
            device_info['Location']['Floor'],
            expected['Location']['Floor'])
        self.assertEquals(
            device_info['Location']['Room'],
            expected['Location']['Room'])
        self.assertEquals(
            device_info['OperatingSystem']['Name'],
            expected['OperatingSystem']['Name'])
        self.assertEquals(
            device_info['OperatingSystem']['Version'],
            expected['OperatingSystem']['Version'])
        self.assertEquals(
            device_info['UserPerson']['Name'],
            expected['UserPerson']['Name'])
        self.assertEquals(
            device_info['UserPerson']['FirstName'],
            expected['UserPerson']['FirstName'])
        self.assertEquals(
            device_info['ResponsiblePerson']['FirstName'],
            expected['ResponsiblePerson']['FirstName'])
        self.assertEquals(
            device_info['ResponsiblePerson']['Name'],
            expected['ResponsiblePerson']['Name'])
        self.assertEquals(device_info['IPv6Ready'], expected['IPv6Ready'])
        self.assertEquals(
            device_info['NetworkInterfaceCards'][0]['HardwareAddress'],
            expected['NetworkInterfaceCards'][0]['HardwareAddress'])
        self.assertEquals(
            device_info['Interfaces'][0]['IPAddress'],
            expected['Interfaces'][0]['IPAddress'])
        self.assertEquals(
            device_info['Interfaces'][0]['InternetConnectivity'],
            expected['Interfaces'][0]['InternetConnectivity'])
        # FIXME: alias not being set on creation
        #self.assertEquals(
        #    device_info['Interfaces'][0]['IPAliases'],
        #    expected['Interfaces'][0]['IPAliases'])
        for k in ('Zone', 'Manufacturer', 'Model', 'GenericType',
                  'Tag', 'Description'):
            if device_info[k] and expected[k]:
                self.assertEquals(device_info[k].lower(), expected[k].lower())
            else:
                self.assertEquals(device_info[k], expected[k])

    def _assert_connect(self, device):
        ip_addr = device.addresses['CERN_NETWORK'][0]['addr']
        resp = 1
        for _ in range(1, 10):
            resp = os.system(
                "/usr/bin/ping -c 1 -W 1 %s > /dev/null" % ip_addr)
            if resp == 0:
                break
            time.sleep(10)
        self.assertEquals(resp, 0)
        # FIXME(OS-2741): add ipv6 check

    def test_create(self):
        """Test for vm creation."""
        self._delete_vm('neutron-plugin-vm-create')

        device = self._get_vm("neutron-plugin-vm-create-%s" % START_TIME)
        self._create_vm(device)

        result = self.nova.servers.list(
            search_opts={'name': device['name']})
        self._assert_vm(result[0], device)

    def test_create_withlandbmetadata(self):
        """Test for vm creation with landb metadata."""
        name = "neutron-plugin-vm-create-landb-%s" % START_TIME
        meta = {
            'cern-services': 'false',
            'landb-description': 'vm landb',
            'landb-mainuser': 'svcbuild',
            'landb-responsible': 'svcbuild',
            'landb-os': 'Linux',
            'landb-osversion': '7',
            'landb-alias': "%s-alias" % name,
            'landb-ipv6ready': 'false',
            'landb-internet-connectivity': 'false',
        }

        # create the device
        self._delete_vm('neutron-plugin-vm-create-landb')
        device = self._get_vm(name, meta=meta)
        self._create_vm(device)

        # check device metadata is correct
        result = self.nova.servers.list(
            search_opts={'name': device['name']})
        device_info = _sample_device_info(
            result[0], description='vm landb',
            connectivity=False,
            alias=[str("%s-alias" % name).upper()])
        self._assert_vm(result[0], device)
        self._assert_landb(result[0], device_info)
        self._assert_connect(result[0])

    def test_update_landbmetadata(self):
        """Test for vm creation and update of landb metadata."""
        name = "neutron-plugin-vm-update-landb-%s" % START_TIME
        meta = {
            'cern-services': 'false',
            'landb-description': 'vm update landb',
            'landb-mainuser': 'svcbuild',
            'landb-responsible': 'svcbuild',
            'landb-os': 'Linux',
            'landb-osversion': '7',
            'landb-alias': "%s-alias" % name,
            'landb-ipv6ready': 'false',
        }

        # create the device
        self._delete_vm('neutron-plugin-vm-update-landb')
        device = self._get_vm(name, meta=meta)
        self._create_vm(device)

        # check the device metadata is correct
        result = self.nova.servers.list(
            search_opts={'name': device['name']})
        device_info = _sample_device_info(
            result[0], description='vm update landb',
            alias=[str("%s-alias" % name).upper()])
        self._assert_vm(result[0], device)
        self._assert_landb(result[0], device_info)

        # update a set of device fields, and recheck metadata
        self.nova.servers.set_meta(
            result[0].id, {'landb-ipv6ready': 'true'})
        device_info['IPv6Ready'] = True
        result = self.nova.servers.list(
            search_opts={'name': device['name']})
        self._assert_landb(result[0], device_info)

    def test_create_withprojmetadata(self):
        """
        Test for vm creation with project landb metadata.

        Expects the project being used to have the properties landb
        mainuser and responsible set to the user below.
        """
        name = "neutron-plugin-vm-projlandb-%s" % START_TIME

        # create the device
        self._delete_vm('neutron-plugin-vm-projlandb')
        device = self._get_vm(name)
        self._create_vm(device)

        # check the device metadata is correct
        result = self.nova.servers.list(
            search_opts={'name': device['name']})
        device_info = _sample_device_info(
            result[0],
            respuser={
                'Name': 'CASTRO LEON', 'FirstName': 'JOSE', 'CCID': 711191},
            mainuser={
                'Name': 'CASTRO LEON', 'FirstName': 'JOSE', 'CCID': 711191},
            osversion='UNKNOWN')
        self._assert_vm(result[0], device)
        self._assert_landb(result[0], device_info)

    def test_create_withimgmetadata(self):
        """
        Test for vm creation with image metadata.

        Expects the image used for the test to have properties
        os=Linux and os_version not set (UNKNOWN).
        """
        name = "neutron-plugin-vm-imgmetadata-%s" % START_TIME

        meta = self.meta.update({
            'landb-mainuser': 'svcbuild',
            'landb-responsible': 'svcbuild',
        })

        # create the device
        self._delete_vm('neutron-plugin-vm-imgmetadata')
        device = self._get_vm(name, meta=meta)
        self._create_vm(device)

        # check the device metadata is correct
        result = self.nova.servers.list(
            search_opts={'name': device['name']})
        device_info = _sample_device_info(
            result[0], osversion='UNKNOWN')
        self._assert_vm(result[0], device)
        self._assert_landb(result[0], device_info)

    def test_create_bootimgcreatevol(self):
        """
        Test for vm creation from image, creating a volume.

        Expects the image used for the test to have properties
        os=Linux and os_version not set (UNKNOWN).
        """
        name = "neutron-plugin-vm-bootimgcreatevol-%s" % START_TIME

        meta = self.meta.update({
            'landb-mainuser': 'svcbuild',
            'landb-responsible': 'svcbuild',
        })

        # create the device
        self._delete_vm('neutron-plugin-vm-bootimgcreatevol')
        device = self._get_vm(
            name, meta=meta,
            blk_mapping=[{
                'boot_index': '1',
                'uuid': '4fe0df1b-fbb5-4d67-a011-6a394855570c',
                'source_type': 'image',
                'destination_type': 'volume',
                'volume_size': '10',
                'delete_on_termination': True,
            }])
        self._create_vm(device)

        # check the device metadata is correct
        result = self.nova.servers.list(
            search_opts={'name': device['name']})
        device_info = _sample_device_info(
            result[0], osversion='UNKNOWN')
        self._assert_vm(result[0], device)
        self._assert_landb(result[0], device_info)
