# Copyright 2015 Meh, Inc.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#
# pylint: skip-file

"""add cern tables

Revision ID: 6ab77ed4850
Revises: start_networking_cern
Create Date: 2015-11-19 16:44:17.070478

"""

# revision identifiers, used by Alembic.
revision = '6ab77ed4850'
down_revision = 'start_networking_cern'

from alembic import op
import sqlalchemy as sa



def upgrade():

    op.create_table(
	'cern_clusters',
	sa.Column('id', sa.String(length=36), nullable=False, primary_key=True),
	sa.Column('project_id', sa.String(length=255), nullable=True),
	sa.Column('name', sa.String(length=255), nullable=False,
		  unique=True))

    op.create_table(
        'cern_subnet_clusters',
        sa.Column('subnet_id', sa.String(length=36), sa.ForeignKey('subnets.id'),
                  primary_key=True, unique=True),
        sa.Column('cluster_id', sa.String(length=36), sa.ForeignKey('cern_clusters.id'),
		  primary_key=True),
        sa.ForeignKeyConstraint(['subnet_id'], ['subnets.id']))
#        sa.PrimaryKeyConstraint('subnet_id'))
