from neutron.plugins.ml2.common import exceptions as ml2_exc
from novaclient import client as nova_client
from glanceclient import Client as glance_client


from networking_cern.common import cernlib as cern
from networking_cern.plugins.ml2.drivers.cern.keystone import Keystone

from oslo_config import cfg
from oslo_log import log

LOG = log.getLogger(__name__)
CONF = cfg.CONF

def _get_os(sess, metadata, instance, dhcp_opts, region_name):
    os_name = 'UNKNOWN'
    os_version = 'UNKNOWN'

    image_client = glance_client('2', session=sess,
                                 region_name=region_name)
    image = None
    if 'id' in instance.image:
        image = image_client.images.get(instance.image['id'])

    if 'landb-os' in metadata.keys():
        os_name = metadata['landb-os']
    elif image and 'os' in image:
        os_name = image['os']
    elif dhcp_opts and 'os' in dhcp_opts:
        os_name = dhcp_opts['os']

    if 'landb-osversion' in metadata.keys():
        os_version = metadata['landb-osversion']
    elif image and 'os_version' in image.keys():
        os_version = image['os_version']
    elif dhcp_opts and 'os_version' in dhcp_opts:
        os_version = dhcp_opts['os_version']

    return {'Name': os_name, 'Version': os_version}

def _get_mainuser(identity_client, ldap_client, instance):
    mainuser = None
    user_id = None
    egroup_id = None

    # start with the user id of the instance (lowest priority)
    tmp_user = ldap_client.user_exists(instance.user_id)
    if tmp_user:
        user_id = tmp_user

    # overwrite with the project main user if defined (medium)
    tmp_user = identity_client.get_project_mainuser(instance.tenant_id)
    if tmp_user:
        user_id = ldap_client.user_exists(tmp_user)
        egroup_id = ldap_client.egroup_exists(tmp_user)

    # overwrite with instance main user metadata if defined (highest)
    if 'landb-mainuser' in instance.metadata.keys():
        tmp_user = instance.metadata['landb-mainuser']
        user_id = ldap_client.user_exists(tmp_user)
        egroup_id = ldap_client.egroup_exists(tmp_user)

    # prefer user, otherwise egroup
    if user_id:
        mainuser = {'PersonID': user_id}
    elif egroup_id:
        mainuser = {'FirstName': 'E-GROUP', 'Name': egroup_id}
    else:
        LOG.error("Failed to find main user '%s' " % mainuser)
        raise ml2_exc.MechanismDriverError(method='landb-mainuser')

    return mainuser

def _get_projectresp(identity_client, ldap_client, instance):
    respuser = None
    user_id = None
    egroup_id = None

    # start with the user id of the instance (lowest priority)
    tmp_user = ldap_client.user_exists(instance.user_id)
    if tmp_user:
        user_id = tmp_user

    # overwrite with the project responsible if defined (medium priority)
    tmp_user = identity_client.get_project_responsible(instance.tenant_id)
    if tmp_user:
        user_id = ldap_client.user_exists(tmp_user)
        egroup_id = ldap_client.egroup_exists(tmp_user)

    # overwrite with the instance responsible metadata if defined (highest)
    if 'landb-responsible' in instance.metadata.keys():
        tmp_user = instance.metadata['landb-responsible']
        user_id = ldap_client.user_exists(tmp_user)
        egroup_id = ldap_client.egroup_exists(tmp_user)

    # prefer user, otherwise egroup
    if user_id:
        respuser = {'PersonID': user_id}
    elif egroup_id:
        respuser = {'FirstName': 'E-GROUP', 'Name': egroup_id}
    else:
        LOG.error("cern-landb: failed to find responsible '%s' "
                    % respuser)
        raise ml2_exc.MechanismDriverError(method='landb-responsible')

    return respuser

def _get_description(instance):
    description = ""
    if 'landb-description' in instance.metadata.keys():
        description = instance.metadata['landb-description']
    return description

def _get_alias(instance):
    alias = []
    if 'landb-alias' in instance.metadata.keys():
        alias = [instance.metadata['landb-alias']]
    return alias

def _get_connectivity(instance):
    if 'landb-internet-connectivity' in instance.metadata.keys() \
        and instance.metadata['landb-internet-connectivity'].lower() \
        == 'false':
        return False
    return True

def _get_ipv6ready(instance):
    if 'landb-ipv6ready' in instance.metadata.keys() \
        and instance.metadata['landb-ipv6ready'].lower() \
        == 'true':
        return True
    return False

def _get_set(instance):
    landb_set = None
    if 'landb-set' in instance.metadata.keys():
        landb_set = instance.metadata['landb-set'].lower()
    return landb_set

def _get_instance(sess, instance_id, region_name):
    compute_client = nova_client.Client('2', session=sess,
                                        region_name=region_name)
    return compute_client.servers.get(instance_id)

def _get_instance_metadata(sess, instance, dhcp_opts, region_name):
    metadata = instance.metadata

    metadata['os'] = _get_os(sess, metadata, instance, dhcp_opts, region_name)

    ldap_client = cern.Xldap(url=CONF.CERN.xldap_url)
    identity_client = Keystone(sess)
    metadata['responsible'] = _get_projectresp(
        identity_client, ldap_client, instance)
    metadata['mainuser'] = _get_mainuser(
        identity_client, ldap_client, instance)

    if metadata['mainuser'] is None or metadata['responsible'] is None:
        LOG.error("cern-landb: failed to find main user and responsible")
        raise ml2_exc.MechanismDriverError(method='mainuser')

    metadata['description'] = _get_description(instance)

    metadata['connectivity'] = _get_connectivity(instance)

    metadata['ipv6ready'] = _get_ipv6ready(instance)

    metadata['alias'] = _get_alias(instance)

    return metadata

def _parse_landb_alias_and_set_string(raw_string, hostname):
    """Parses a string from the nova property for landb- properties.

    Input is the raw string value of the landb property and the hostname to
    use by default.
    The output is a dict with the key being the interface names and the value
    being a set of upper case strings with the values belonging to the
    interface.
    This function also removes duplicates and empty strings.
    """
    parsed_dict = {}
    for value in raw_string.split(';'):
        ifname = hostname + ".CERN.CH"
        if ':' in value:
            ifname = value.split(':')[0].strip()
            ifname = ifname if '.' in ifname else ifname + ".CERN.CH"
            value = value.split(':')[1]

        entries = set(v.strip().upper() for v in value.split(','))
        if ifname.upper() in parsed_dict:
            entries.update(parsed_dict[ifname.upper()])

        # Do not keep empty entries
        if '' in entries:
            entries.remove('')
        parsed_dict[ifname.upper()] = entries
    return parsed_dict