# CERN LanDB neutron plugin

## Testing

Tests are available using python unittest. They cover creation and management of VM metadata and consistency checks with landb.

Example to run as 'svcbuild' in teststack (source a different openstack environment to use another account/endpoint):

```
export OS_AUTH_URL="https://keystonecmp.cern.ch/main/v3"
export OS_IDENTITY_API_VERSION="3"
export OS_REGION_NAME="cern"
export OS_DEFAULT_DOMAIN_NAME="default"
export OS_USERNAME="svcbuild"
export OS_PROJECT_NAME="Cloud CI"
export OS_PASSWORD="<svc-build-password>"

export IMG='CC7 Base - x86_64 [2015-06-12]'
export LANDB_PASSWORD='<landb-password>'

python setup.py test

```

You can also run a single test:

```
python unittest -m networking_cern.tests.creation_test.TestDevice.test_create_withprojmetadata
```

## Development

The main branch is `master`.

Development is done by branching from `master`.

When development is ready to review, a merge requests is created.

Changes are tracked in the `[Unreleased]` section of `CHANGELOG.md`. New changes should update this section accordingly.

## Release

New releases are done by:

* Creating a merge request that:
  * Updates CHANGELOG.md to move the `[Unreleased]` section to a new version.
  * Updates Python software version.
* When merged, a tag is created with the format `vX.Y.Z`.

## Distribution

### Docker images

Docker images are produced automatically within this project using the following criteria:

* **noop**: builds are triggered automatically for all the development branches. This step helps validating that docker image creation works but it doesn't push containers to the registry.
* **merge request**: images tagged with `mr_$ID` are produce for opened merge requests. This should help testing new features ready to review/merge.
* **test**: a `test` tag is created for every new change that makes it into the `master` branch. This tag can be used for battle testing
* **tag**: a new tagged version is produced when a tag is created in the repository.

### RPM

The RPM distribution is implemented in a separate repository called [openstack-neutron-distgit](https://gitlab.cern.ch/cloud-infrastructure/openstack-neutron-cern-distgit).

The main contract between the two repos is that this software must be released using the tag naming pattern: `vX.Y.Z`.
