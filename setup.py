
#!/usr/bin/env python
# Copyright (c) 2013 Hewlett-Packard Development Company, L.P.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# THIS FILE IS MANAGED BY THE GLOBAL REQUIREMENTS REPO - DO NOT EDIT


from setuptools import setup, find_packages

PROJECT = 'networking_cern'
VERSION = '0.16.0'

try:
    long_description = open('README.rst', 'r').read()
except IOError:
    long_description = ''

setup(
    name=PROJECT,
    version=VERSION,

    description='Neutron CERN Networking plugin',
    long_description=long_description,

    author='CERN Cloud',
    author_email='cloud-developers@cern.ch',

    url='https://gitlab.cern.ch/cloud-infrastructure/openstack-neutron-cern',
    download_url='https://gitlab.cern.ch/cloud-infrastructure/openstack-neutron-cern/repository/archive.tar.gz?ref=master',

    classifiers=['License :: OSI Approved :: Apache Software License',
                 'Programming Language :: Python',
                 'Programming Language :: Python :: 2',
                 'Programming Language :: Python :: 2.7',
                 'Programming Language :: Python :: 3',
                 'Programming Language :: Python :: 3.2',
                 'Intended Audience :: Developers',
                 'Environment :: Console',
                 ],

    platforms=['Any'],

    scripts=[],

    provides=[],
    install_requires=[
        'neutron>=15.0.0',
        'python-landbclient'
    ],

    namespace_packages=[],
    packages=find_packages(),
    include_package_data=True,

    entry_points={
        'neutron.service_plugins': [
            'cern = networking_cern.plugins.cern.plugin:CERNServicePlugin'
        ],
        'neutron.ml2.mechanism_drivers': [
            'cern_landb = networking_cern.plugins.ml2.drivers.cern.mech_landb:CERNLanDBMechanismDriver'
        ],
        'neutron.db.alembic_migrations': [
            'cern_db = networking_cern.db.migration:alembic_migrations'
        ],
    },

    test_suite='nose.collector',
    tests_require=['nose'],

    zip_safe=False,
)
