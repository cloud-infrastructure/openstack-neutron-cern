# Copyright (c) 2015 CERN
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#
# pylint: disable=no-member

"""Custom CERN exceptions."""

from neutron_lib import exceptions


class CernProjectTargetCell(exceptions.NeutronException):
    """Failure to select an available cell."""
    message = "Failed to select available cell."


class CernDNS(exceptions.NeutronException):
    """Failure to update DNS."""
    message = "Failed to update DNS."


class CernNetwork(exceptions.NeutronException):
    """Inconsistency detected on the network."""
    message = "Network inconsistency."


class CernHostnameWrong(exceptions.NeutronException):
    """Invalid hostname given."""
    message = "Invalid hostname."


class CernInvalidHostname(exceptions.NeutronException):
    """Invalid hostname or device already exists."""
    message = "Device already exists or is not a valid hostname."
    code = 404


class CernInvalidUser(exceptions.NeutronException):
    """Invalid user."""
    message = "Invalid user."
    code = 404


class CernInvalidEgroup(exceptions.NeutronException):
    """Invalid egroup."""
    message = "Invalid egroup."
    code = 404


class CernInvalidUserEgroup(exceptions.NeutronException):
    """Invalid user or egroup."""
    message = "Invalid user or egroup."
    code = 404


class CernInvalidDevice(exceptions.NeutronException):
    """Invalid device."""
    message = "Invalid device."
    code = 404


class CernDeviceNotFound(exceptions.NeutronException):
    """Device not found."""
    message = "Device not found."


class CernLanDB(exceptions.NeutronException):
    """Failure to connect to LanDB."""
    message = "Unable to connect to LanDB"


class CernLanDBAuthentication(exceptions.NeutronException):
    """Failure to authenticate to LanDB."""
    message = "Unable to authenticate to LanDB"


class CernLanDBUpdate(exceptions.NeutronException):
    """Failure to update LanDB."""
    message = "Unable to update LanDB"


class CernActiveDirectory(exceptions.NeutronException):
    """Failure to update or contact active directory."""
    message = "Network Error"
