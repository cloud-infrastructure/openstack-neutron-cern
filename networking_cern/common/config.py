#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#

"""This module holds common config definitions for the CERN neutron plugin."""

from oslo_config import cfg

CERN_LDAP_OPTS = [
    cfg.StrOpt('xldap_url',
               default='ldap://xldap.cern.ch', secret=False,
               help='ldap endpoint')
]

CERN_NETWORK_OPTS = [
    cfg.StrOpt('landb_hostname',
               default='', secret=True,
               help='landb hostname'),
    cfg.StrOpt('landb_port',
               default='443', secret=True,
               help='landb port'),
    cfg.StrOpt('landb_protocol',
               default='https', secret=True,
               help='landb protocol'),
    cfg.StrOpt('landb_username',
               default='', secret=True,
               help='landb username'),
    cfg.StrOpt('landb_password',
               default='', secret=True,
               help='landb password'),
    cfg.IntOpt('landb_version',
               default=6, secret=True,
               help='landb version'),
    cfg.StrOpt('landb_manager',
               default='', secret=True,
               help='landb manager'),
    cfg.BoolOpt('verify',
                default=True, secret=True,
                help='verify TLS'),

    cfg.BoolOpt('dry_run',
                default=False, secret=False,
                help='Only log LanDB operations (only refactored version)')
]

CERN_TUNABLE_OPTS = [
    cfg.BoolOpt('use_segments',
                default=False, secret=False,
                help='Specify whether the setup is using Neutron segments'),
    cfg.BoolOpt('landb_refactor',
                default=False, secret=False,
                help='Whether the plugin should use the new model of LanDB '
                     'integration')
]

CERN_METADATA_OPTS = [
    cfg.StrOpt('metadata_host',
               default='127.0.0.1', secret=False,
               help='Host of the metadata server for this hypervisor'),
    cfg.StrOpt('metadata_port',
               default='8775', secret=False,
               help='Port of metadata server for this hypervisor')
]

cfg.CONF.register_opts(CERN_LDAP_OPTS, 'CERN')
cfg.CONF.register_opts(CERN_NETWORK_OPTS, 'CERN')
cfg.CONF.register_opts(CERN_METADATA_OPTS, 'CERN')
cfg.CONF.register_opts(CERN_TUNABLE_OPTS, 'CERN')