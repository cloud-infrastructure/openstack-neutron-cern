#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#
# pylint: disable=undefined-variable,no-member

"""Holds exceptions for CERN subnet clsuter extension."""

from neutron_lib import exceptions


class ClusterExists(exceptions.NeutronException):
    """Subnet cluster already exists."""
    message = "Subnet cluster with name %(name)s already exist."


class ClusterNotFound(exceptions.NotFound):
    """Cluster could not be found."""
    message = "Cluster %(cluster_id)s could not be found."


class SubnetInOtherCluster(exceptions.NeutronException):
    """Subnet is already assigned to a different cluster."""
    message = "Subnet %(subnet)s is already assigned to another cluster."


class SubnetNotInCluster(exceptions.NotFound):
    """Subnet is not in cluster."""
    message = "Cluster %(cluster)s does not contain subnet %(subnet)s."


class SubnetAlreadyInCluster(exceptions.NeutronException):
    """Subnet is already in cluster."""
    message = "Cluster %(cluster)s already contains subnet %(subnet)s."
