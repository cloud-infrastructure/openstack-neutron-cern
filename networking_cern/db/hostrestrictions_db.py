#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#
# pylint: disable=no-member,too-many-ancestors,unused-import

"""
This module holds db interaction methods for the CERN host restrictions
extension.
"""

import random
import netaddr

from neutron_lib import exceptions
from neutron.db import models_v2
from neutron.db import db_base_plugin_v2 as base_db
from oslo_config import cfg
from oslo_log import log as logging

from landbclient import client as landbclient

from networking_cern.common import config
from networking_cern.extensions import hostrestrictions

from neutron_lib.api import attributes

CONF = cfg.CONF
LOG = logging.getLogger(__name__)


def _number_of_allocated_ips(context, subnet_id):
    return context.session.query(
        models_v2.IPAllocation).filter_by(subnet_id=subnet_id).count()


def _number_of_free_ips(context, subnet_id):
    pools = context.session.query(
        models_v2.IPAllocationPool).filter_by(subnet_id=subnet_id).all()
    no_of_addresses_in_subnet = 0
    for pool in pools:
        ip_range = netaddr.IPRange(pool['first_ip'], pool['last_ip'])
        no_of_addresses_in_subnet += ip_range.size

    alloc_count = _number_of_allocated_ips(context, subnet_id)

    return no_of_addresses_in_subnet - alloc_count


def _filter_available_subnets(context, all_subnets):
    subnets_v4 = []
    subnets_v6 = []
    query_subnets = context.session.query(
        models_v2.Subnet.id, models_v2.Subnet.ip_version,
        models_v2.Subnet.name).filter(
            models_v2.Subnet.id.in_(all_subnets)).all()

    for subnet in query_subnets:
        if _number_of_free_ips(context, subnet[0]) > 0:
            if subnet[1] == 4:
                subnets_v4.append(subnet)
            else:
                subnets_v6.append(subnet)

    return subnets_v4, subnets_v6


def _find_least_available_subnet(context, subnets):
    if subnets:
        la_subnet = subnets[0]
        min_count = _number_of_free_ips(context, subnets[0][0])
        for subnet in subnets:
            free_count = _number_of_free_ips(context, subnet[0])
            if free_count < min_count and free_count > 0:
                la_subnet = subnet
                min_count = free_count
        return la_subnet[0]
    return None


def _find_most_available_subnet(context, subnets_v4, subnets_v6):
    ma_subnet_v4_id = None
    ma_subnet_v6_id = None

    if subnets_v4:
        ma_subnet_v4_name = None
        v4_list = [(s, _number_of_free_ips(context, s[0])) for s in subnets_v4]
        v4_sorted = sorted(v4_list, key=lambda x: x[1], reverse=True)
        ma_subnet_v4 = v4_sorted[0][0]
        ma_subnet_v4_id = ma_subnet_v4[0]
        ma_subnet_v4_name = ma_subnet_v4[2]

        for subnet_v6 in subnets_v6:
            if ma_subnet_v4_name in subnet_v6[2]:
                ma_subnet_v6_id = subnet_v6[0]

    return ma_subnet_v4_id, ma_subnet_v6_id


def _get_random_subnet(subnets_v4, subnets_v6):
    ar_subnet_v4 = random.choice(subnets_v4)
    ar_subnet_v6 = None
    for subnet in subnets_v6:
        if ar_subnet_v4[2] in subnet[2]:
            ar_subnet_v6 = subnet

    if not ar_subnet_v4 or not ar_subnet_v6:
        LOG.error("CERN: failed to find available subnet :: %s :: %s"
                  % (ar_subnet_v4, ar_subnet_v6))
        raise NoAvailSubnetFound()

    return ar_subnet_v4[0], ar_subnet_v6[0]


class HostRestrictionsDbMixin(hostrestrictions.HostRestrictionsPluginBase,
                              base_db.NeutronDbPluginV2):
    """
    HostRestrictionsDbMixin is the database part of the host restrictions
    extension.
    """

    def _make_host_dict(self, hostname, all_subnets, available_subnets,
                        ar_subnet, ar_subnet_v6, ma_subnet_v4, ma_subnet_v6, la_subnet, fields):
        res = {'hostname': hostname,
               'all_subnets': all_subnets,
               'available_subnets': [s[0] for s in available_subnets],
               'available_random_subnet': ar_subnet,
               'available_random_subnet_v6': ar_subnet_v6,
               'most_available_subnet': ma_subnet_v4,
               'most_available_subnet_v4': ma_subnet_v4,
               'most_available_subnet_v6': ma_subnet_v6,
               'least_available_subnet': la_subnet}

        if fields:
            res = {key: item for key, item in res.items()
                    if key in fields}
        return attributes.populate_project_info(res)

    def _get_subnets_for_host(self, context, hostname):
        landb_username = CONF.CERN.landb_username
        landb_password = CONF.CERN.landb_password
        landb_hostname = CONF.CERN.landb_hostname
        landb_port = CONF.CERN.landb_port
        landb_protocol = CONF.CERN.landb_protocol

        client_landb = landbclient.LanDB(username=landb_username,
                                         password=landb_password,
                                         host=landb_hostname,
                                         port=landb_port,
                                         protocol=landb_protocol)
        clusters = client_landb.vm_get_cluster_membership(hostname)
        subnets = []
        for cluster_name in clusters:
            cluster = self.get_cluster_by_name(context, cluster_name)
            if cluster is None:
                LOG.warn(
                    "CERN: Cluster %s for host %s is unknown to Neutron"
                    % (cluster_name, hostname))
            if cluster is not None:
                subnets.extend(cluster['subnets'])
        if not subnets:
            LOG.error(
                "CERN: No usable subnet found for host %s" % hostname)
            raise NoSubnetFound(hostname=hostname)
        return subnets

    def get_host(self, context, host_id, fields=None):
        all_subnets = self._get_subnets_for_host(context, host_id)
        available_subnets_v4, available_subnets_v6 = (
            _filter_available_subnets(context, all_subnets))

        ar_subnet_v4, ar_subnet_v6 = _get_random_subnet(
            available_subnets_v4, available_subnets_v6)

        ma_subnet_v4, ma_subnet_v6 = _find_most_available_subnet(
            context, available_subnets_v4, available_subnets_v6)
        least_available_subnet = _find_least_available_subnet(
            context, available_subnets_v4)
        return self._make_host_dict(host_id,
                                    all_subnets,
                                    available_subnets_v4,
                                    ar_subnet_v4,
                                    ar_subnet_v6,
                                    ma_subnet_v4,
                                    ma_subnet_v6,
                                    least_available_subnet,
                                    fields)


# Exceptions
# FIXME: move this to common exceptions
class NoSubnetFound(exceptions.NeutronException):
    """Error for no subnet found."""
    message = "No usable subnet found for host %(hostname)s"


class NoAvailSubnetFound(exceptions.NeutronException):
    """Error for no available subnet found."""
    message = "No subnet is available for the host"
